<?php

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $name = strip_tags(trim($_POST["name"]));
        $name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $message = trim($_POST["message"]);
        $subj = ucfirst(trim($_POST["subject"]));
        $pid = trim($_POST["pid"]);
        $mid = trim($_POST["mid"]);
        $version = trim($_POST["version"]);
        $poiLoc = trim($_POST["poiLoc"]);
        $userIP = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP);
        $directory = explode(".",$_SERVER['HTTP_HOST']);

        $isDickHead = false;
        $badwords = [
          "fuck",
          "nigger",
          "cunt",
          "pussy",
          " nig",
          "nigs"
        ];

        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        foreach($badwords as $string)
        {
          if(strpos($name, $string) !== false)
          {
            $isDickHead = true;
            break;
          }
        }

        foreach($badwords as $string)
        {
          if(strpos($email, $string) !== false)
          {
            $isDickHead = true;
            break;
          }
        }

        foreach($badwords as $string)
        {
          if(strpos($message, $string) !== false)
          {
            $isDickHead = true;
            break;
          }
        }

        if($isDickHead){
          http_response_code(418);
          echo "Try washing out that mouth and submitting again. GSO!";
          return;
        }

        // Set the recipient email address.
        $recipient = "feedback@darkfall-info.com";

        // Set the email subject.
        $subject = (($directory[0] === 'staging') ? '[STAGING] ' : '') . "DF Info Map: $subj - $name";

        // Build the email content.
        $email_content = "Name: $name\n";
        $email_content .= "Email: $email\n";
        $email_content .= "Subject: $subj\n\n";
        $email_content .= "Message:\n$message\n\n";
        if(!empty($pid)){
            $email_content .= "PID#: $pid\n\n";
        }
        if(!empty($mid)){
            $email_content .= "MID#: $mid\n\n";
        }
        if(!empty($poiLoc)){
            $email_content .= "POI Location: http://wiki.riseofagon.info/map/#$poiLoc\n\n";
        }
        if(!empty($version)){
          $email_content .= "Map Version: $version";
        }

        // add user's IP
        $email_content .= "\n\n\nIP: $userIP";

        // Build the email headers.
        $email_headers = "From: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Thank You! Your message has been sent.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>
