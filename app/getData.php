<?php
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  require 'ragdick/config.php';

  $link = mysqli_connect($servername, $username, $password);

  if (!$link = mysqli_connect($servername, $username, $password)) {
      echo 'Could not connect to mysql';
      exit;
  }

  if (!mysqli_select_db($link, $database)) {
      echo 'Could not select database';
      exit;
  }

  $mobSql = "SELECT * FROM `mobs`";
  $mobResult = mysqli_query($link, $mobSql);

  $poiSql = "SELECT * FROM `pois`";
  $poiResult = mysqli_query($link, $poiSql);

  $rows = array();
  while($r = mysqli_fetch_assoc($mobResult)) {
      $rows['mobs'][] = $r;
  }
  while($s = mysqli_fetch_assoc($poiResult)) {
    $rows['pois'][] = $s;
  }
  print json_encode($rows);

?>
