<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <title>Mobs</title>
        <meta name="description" content="ENTER DESCRIPTION">
        <meta http-equiv="refresh" content="10">
    </head>
    <style type="text/css">
    body {
      background: #ccc;
      width: 70%;
      margin: 50px auto 0;
    }

    table {
      width: 100%;
      border: 1px solid black;
    }

    table th {
      background: black;
      padding: 2px 3px;
      color: white;
      font-weight: bold;
    }

    table td {
        padding: 2px;
    }

    table tr {
        background: white;
    }

    table tr:nth-child(2) {
        background: #ccc;
    }
    </style>
    <body>
    <?php

    require 'config.php';

    //$link = mysql_connect($servername, $username, $password);

    if (!$link = mysql_connect($servername, $username, $password)) {
        echo 'Could not connect to mysql';
        exit;
    }

    if (!mysql_select_db($database, $link)) {
        echo 'Could not select database';
        exit;
    }

    $sql = "SELECT * FROM `mobs` LIMIT 0, 30 ";
    $result = mysql_query($sql, $link);

    if (!$result) {
        echo "DB Error, could not query the database\n";
        echo 'MySQL Error: ' . mysql_error();
        exit;
    }

    $num_rows = mysql_num_rows($result);

    if($num_rows === 0){
        echo '<table cellpadding=0 cellspacing=0 border=1><tr><td>No Data Yet...WTF you doing Rag?!</td></tr></table>';
    }else{
        echo '<table cellpadding=0 cellspacing=0 border=1><tr><th>ID</th><th>NAME</th><th>DIFFICULTY</th><th>SKIN</th><th>LOOT</th><th>NOTES</th></tr>';
        while ($row = mysql_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>' . $row['id'] . '</td>';
            echo '<td>' . $row['name'] . '</td>';
            echo '<td>' . $row['difficulty'] . '</td>';
            echo '<td>' . $row['skin'] . '</td>';
            echo '<td>' . $row['loot'] . '</td>';
            echo '<td>' . $row['notes'] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }


    mysql_free_result($result);
    ?>
    </body>
</html>
