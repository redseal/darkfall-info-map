'use strict';
(function(){
  var selected = null, // Object of the element to be moved
  xPos = 0, yPos = 0, // Stores x & y coordinates of the mouse pointer
  xElem = 0, yElem = 0; // Stores top, left values (edge) of the element

  // Will be called when user starts dragging an element
  function dragInit(elem) {
    // Store the object of the element which needs to be moved
    selected = elem;
    xElem = xPos - selected.offsetLeft;
    yElem = yPos - selected.offsetTop;
  }

  // Will be called when user dragging an element
  function moveElem(e) {
    xPos = document.all ? window.event.clientX : e.pageX;
    yPos = document.all ? window.event.clientY : e.pageY;
    if (selected !== null) {
      selected.style.left = (xPos - xElem) + 'px';
      selected.style.top = (yPos - yElem) + 'px';
    }
  }

  // Destroy the object when we are done
  function destroy() {
    if(typeof (Storage) !== 'undefined' && selected) {
      localStorage.setItem(selected.getAttribute('id'), (xPos - xElem) + 'px' + '|' + (yPos - yElem) + 'px');
      // console.log((xPos - xElem) + 'px' + '|' + (yPos - yElem) + 'px');
    }
    selected = null;
  }

  // Bind the functions...
  var dragWindows = document.getElementsByClassName('draggable');
  // console.log(dragWindows, typeof dragWindows);
  [].forEach.call(dragWindows, function (el) {
    if(typeof (Storage) !== 'undefined'){
      var storedCoords = localStorage.getItem(el.getAttribute('id'));
      if(storedCoords){
        // console.log(storedCoords);
        el.style.left = storedCoords.split('|')[0];
        el.style.top = storedCoords.split('|')[1];
      }
    }
    el.onmousedown = function () {
      dragInit(this);
      return false;
    };
  });

  document.onmousemove = moveElem;
  document.onmouseup = destroy;
})();
