'use strict';

(function($){
    // const cb = new Date().getTime();
    const repeatOnXAxis = false; // Do we need to repeat the image on the X-axis? Most likely you'll want to set this to false
    const imgPath = 'data/tiles/';

    // location.queryString = {};
    // location.search.substr(1).split('&').forEach(function (pair) {
    //     if (pair === '') {
    //       return;
    //     }
    //     var parts = pair.split('=');
    //     location.queryString[parts[0]] = parts[1] &&
    //         decodeURIComponent(parts[1].replace(/\+/g, ' '));
    // });

    let autoShowLabels = document.getElementById('autoshow').checked;

    if(localStorage.getItem('df-info-autoshow') === 'false') {
      $('#autoshow').click();
      autoShowLabels = false;
    }

    /*eslint-disable no-unused-vars*/
    let pois = [];
    let mobs = [];
    let map;
    let userMarker;

    let data = {
      'mobs': {
        active: false,
        items: []
      },
      'mobs-easy': {
        active: false,
        items: []
      },
      'mobs-medium': {
        active: false,
        items: []
      },
      'mobs-hard': {
        active: false,
        items: []
      },
      'mobs-boss': {
        active: false,
        items: []
      },
      'pois': {
        active: false,
        items: []
      },
      'banks': {
        active: false,
        items: []
      },
      'playerCities': {
        active: false,
        items: []
      },
      'hamlets': {
        active: false,
        items: []
      },
      'cities': {
        active: false,
        items: []
      },
      'villages': {
        active: false,
        items: []
      },
      'dungeons': {
        active: false,
        items: []
      },
      'portals': {
        active: false,
        items: []
      },
      'portalNetwork': {
        active: false,
        items: []
      },
      'search': {
        active: false,
        items: []
      }
    };
    let searchWaitTime;
    /*eslint-enable no-unused-vars*/

    var stopShowingSearching = () => {
      $('.loading-overlay').remove();
      $('.searching').removeClass('searching').addClass('glyphicon-search');
    };

    var showMarkers = function(arr, toShow){
      var markerArray = arr.items;
      var theMap = (toShow) ? map : null;

      // if its a portal entrance, it will have a subpoi
      // we will then detect this and show the portal
      // network overlay as well
      if(markerArray === data.portals.items){
        showMarkers(data.portalNetwork, toShow);
      }

      for(var i = 0; i < markerArray.length; i++){
          markerArray[i].setMap(theMap);
          if(autoShowLabels && !markerArray[i].strokeColor){
            markerArray[i].label.setMap(theMap);
          }
      }
      stopShowingSearching();
    };

    var hashCheck = function(){
      var hash = window.location.hash;
      if(hash) {
        if(window.ga){
          // ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);
          window.ga('send', 'event', 'CustomMarker', 'load', hash);
        }
        hash = hash.substring(1).split('%7C');
        let lat = Number(hash[0]);
        let lng = Number(hash[1]);
        placeMarker(new google.maps.LatLng(lat, lng));
      }
    };

    var searchPOIs = function(str){
      if(window.ga){
        // ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);
        window.ga('send', 'event', 'Search', 'input', str.toLowerCase());
      }

      showMarkers(data.search, false);
      data.search.items = [];

      // first we will check to see if there are any active overlays
      if($('.sidebar-filters input:checked')){
        $('.sidebar-filters input:checked').parent().click();
        $('.sidebar-search input').focus();
      }

      // remove all highlighted words
      $('span.searched').each(function(){
        let txt = $(this).text();
        $(this).replaceWith(txt);
      });

      str = str.toLowerCase();

      mobs.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.loot && item.loot.toLowerCase().match(str)) || (item.skin && item.skin.toLowerCase().match(str))) {
          const matchingMob = item;
          data['mobs-easy'].items.forEach(function(mobMarker){
            let subPois = mobMarker.subpoi.replace('[', '').replace(']', '').split(',');
            for(var i = 0; i < subPois.length; i++) {
              if(subPois[i] === matchingMob.id){
                data.search.items.push(mobMarker);
              }
            }
          });
          data['mobs-medium'].items.forEach(function(mobMarker){
            let subPois = mobMarker.subpoi.replace('[', '').replace(']', '').split(',');
            for(var i = 0; i < subPois.length; i++) {
              if(subPois[i] === matchingMob.id){
                data.search.items.push(mobMarker);
              }
            }
          });
          data['mobs-hard'].items.forEach(function(mobMarker){
            let subPois = mobMarker.subpoi.replace('[', '').replace(']', '').split(',');
            for(var i = 0; i < subPois.length; i++) {
              if(subPois[i] === matchingMob.id){
                data.search.items.push(mobMarker);
              }
            }
          });
          data['mobs-boss'].items.forEach(function(mobMarker){
            let subPois = mobMarker.subpoi.replace('[', '').replace(']', '').split(',');
            for(var i = 0; i < subPois.length; i++) {
              if(subPois[i] === matchingMob.id){
                data.search.items.push(mobMarker);
              }
            }
          });
        }
      });

      pois.forEach(function(item){
        if(item.id.toString() === str){
          const pidPoint = new google.maps.LatLng(item.y, item.x);
          if(userMarker){
            userMarker.setPosition(pidPoint);
          }else{
            placeMarker(pidPoint);
          }
        }
      });

      data.cities.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.notes && item.notes.toLowerCase().match(str)) || item.pid.toString() === str) {
          data.search.items.push(item);
        }
      });

      data.playerCities.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.notes && item.notes.toLowerCase().match(str)) || item.pid.toString() === str) {
          data.search.items.push(item);
        }
      });

      data.hamlets.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.notes && item.notes.toLowerCase().match(str)) || item.pid.toString() === str) {
          data.search.items.push(item);
        }
      });

      data.villages.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.notes && item.notes.toLowerCase().match(str)) || item.pid.toString() === str) {
          data.search.items.push(item);
        }
      });

      data.banks.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.notes && item.notes.toLowerCase().match(str)) || item.pid.toString() === str) {
          data.search.items.push(item);
        }
      });

      data.portals.items.forEach(function(item){
        if(item.name.toLowerCase().match(str)) {
          data.search.items.push(item);
        }
      });

      data.pois.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || item.pid.toString() === str){
          data.search.items.push(item);
        }
      });

      data.dungeons.items.forEach(function(item){
        if(item.name.toLowerCase().match(str) || (item.notes && item.notes.toLowerCase().match(str)) || item.pid.toString() === str) {
          data.search.items.push(item);
        }
      });

      showMarkers(data.search, true);
    };

    var onUserInputSearch = () => {
      const inputText = $('.sidebar-search input[type=text]').val();

      if(inputText.length){
        searchPOIs(inputText);
      }else{
        showMarkers(data.search, false);
      }
    };

    var showSearching = () => {
      const inputText = $('.sidebar-search input[type=text]').val();
      if(inputText.length){
        if(!$('.loading-overlay').length){
          $('#map').prepend('<div class="loading-overlay"></div>');
          $('.glyphicon-search').removeClass('glyphicon-search').addClass('searching');
        }
      }
    };

    var checkForActiveOverlays = (toShow) => {
      for(let overlay in data){
        if(data[overlay].active){
          swapLabels(data[overlay].items, toShow);
        }
      }
    };

    var swapLabels = (obj, toShow) => {
      let theMap = (toShow) ? map : null;
      for(let i = 0; i < obj.length; i++){
        obj[i].label.setMap(theMap);
      }
    };

    var setupBinds = function(){
      $('h4.mobs, h4.holdings').on('click', function(){
        let checkBox = $(this).find('span');
        const checkBoxes = $(this).parent().find('input');
        checkBoxes.prop('checked', !checkBoxes.prop('checked')).change();
        if(checkBox.hasClass('glyphicon-unchecked')){
          checkBox.removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        }else{
          checkBox.removeClass('glyphicon-checked').addClass('glyphicon-unchecked');
        }
      });

        $('.sidebar').on('click', function(){
          $(this).addClass('active');
        });

        $('.sidebar').on('click', '.sidebar-close', function(event){
          event.preventDefault();
          event.stopPropagation();
          $('.sidebar').removeClass('active');
          return false;
        });

        $('.sidebar-filters section').on('change', 'input[type=checkbox]', function(){
          let array = data[$(this).data('type')];
          array.active = $(this).is(':checked');
          if(window.ga){
            // ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);
            window.ga('send', 'event', 'Filters', $(this).data('type'), array.active.toString());
          }
          showMarkers(array, array.active);
          return false;
        });

        $('.sidebar-search input[type=text]').keyup(function(){
          showSearching();
          clearTimeout(searchWaitTime);
          searchWaitTime = setTimeout(function(){
            onUserInputSearch();
          }, 1000);
        });

        $('.sidebar-search').on('click', 'button', function(){
          searchPOIs($('.sidebar-search input[type=text]').val());
        });

        $('.sidebar-details').on('click', '.searchable', function(){
          let searchTerm = $(this).text();
          if($(this).text().match(/\[/) !== null){
            const regx = /\[(.*?)\]/g;
            const match = regx.exec(searchTerm.trim());
            searchTerm = match[1];
          }else{
            searchTerm = searchTerm.replace(/\[|\]|\(|\)|[0-9]|\%|-|\?|~/g, '').trim();
          }
          $('.sidebar-search input').val(searchTerm).focus().keyup();
          window.ga('send', 'event', 'Search', 'click', searchTerm.toLowerCase());
        });

        $('.sidebar-details').on('click', '.report', (e) => {
          if(e.currentTarget.dataset.mid){
            $('input[name=mid]').val($(e.currentTarget).data('mid'));
          }else{
            $('input[name=pid]').val($(e.currentTarget).data('pid'));
          }
          $('.pidContainer').show();
          $('#feedbackForm').find('#poiLoc').val('');
          $('.poiLocContainer').hide();
          $('#feedbackForm').modal('show');
        });

        $('#autoshow').on('change', function(){
          autoShowLabels = document.getElementById('autoshow').checked;
          localStorage.setItem('df-info-autoshow', autoShowLabels);
          checkForActiveOverlays(autoShowLabels);
        });

        hashCheck(); // lets see if the user has a deeplink
    };

    var checkForHighlight = function(str, search) {
      str = str.toLowerCase();
      search = search.toLowerCase();
      return (search && str.toLowerCase().match(search)) ? str.toLowerCase().replace(search, '<span class="searched">' + search + '</span>') : str;
    };

    var getSubInfo = function(subs){
      var highlightedWord = $('.sidebar-search input[type=text]').val();
      let subInfo = '';
      subs.forEach(function(sub){
        for (let i = 0; mobs.length > i; i++) {
            if (parseInt(mobs[i].id) === parseInt(sub)) {
                subInfo += '<h3 style="color: #' + mobs[i].difficulty + ';" class=" ' + (mobs[i].levelup ? 'levelup' : '') + '">' + checkForHighlight(mobs[i].name, highlightedWord) + '</h3>';
                if(mobs[i].skin){
                  subInfo += '<span><b>Skin:</b> ' + checkForHighlight(linkifyIt(mobs[i].skin), highlightedWord) + '</span>';
                }
                if(mobs[i].loot){
                  subInfo += '<span><b>Loot:</b> ' + checkForHighlight(linkifyIt(mobs[i].loot), highlightedWord) + '</span>';
                }
                if(mobs[i].notes){
                  subInfo += '<span><b>Notes:</b> ' + checkForHighlight(mobs[i].notes, highlightedWord) + '</span>';
                }
                subInfo += '<button href="#" data-type="mob" data-mid="' + mobs[i].id + '" class="report btn btn-xs btn-warning"><span class="glyphicon glyphicon-warning-sign"></span> Report an issue for ' + mobs[i].name + '</button>';
            }
        }
      });
      return subInfo;
    };

    var linkifyIt = function(str){
      str = '<a class="searchable">' + str + '</a>';
      str = str.replace(/, /g, '</a>, <a class="searchable">');
      return str;
    };

    var updateInformationWindow = function(arr){
      var highlightedWord = $('.sidebar-search input[type=text]').val();
      let content = '';
      if(arr.subpoi){
        if(arr.type === 11){
          content += '<h3>Dungeon Notes:</h3><p>' + checkForHighlight(arr.notes, highlightedWord) + '</p>';
        }
        content += getSubInfo(JSON.parse(arr.subpoi));
      }else{
        content = '<h3 style="color: #' + arr.color + ';">' + checkForHighlight(arr.name, highlightedWord) + '</h3>';
        if(arr.notes){
          content += '<span><b>Notes:</b> ' + checkForHighlight(linkifyIt(arr.notes), highlightedWord) + '</span>';
        }
      }
      // content += '<small>pid: <span id="pid">' + arr.pid + '</span></small>';
      $('[data-type="location"]').data('pid', arr.pid);
      $('.sidebar-details-content').html(content);
      $('.sidebar-details button').show();

      /* max-height for scrollable content */
      // var wHeight = window.innerWidth;
      var sHeight = $('.sidebar').height();
      var cHeight = $('.sidebar-details-content').height();
      var fHeight = $('.sidebar-filters').height();
      var padding = 140; // no clue where this came from. im a loser for not putting a comment of my thoughts sooner.
      var total = cHeight + fHeight + padding;
      if(sHeight - total > 0){
        $('.sidebar-details-content').css({'maxHeight': 'none', 'overflow-y': 'hidden'});
      }else{
        $('.sidebar-details-content').css({'maxHeight': cHeight + (sHeight - total) + 'px', 'overflow-y': 'scroll', 'paddingRight': '18px'});
      }
    };

    var getExitInfo = (subs, id) => {
      for(let i = 0; i < subs.length; i++){
        if(parseInt(subs[i].pid) === id){
          return subs[i];
        }
      }
    };

    var createPortalNetworks = () => {
      // we need to create a polyline and throw
      // it in the portalNetwork container
      for(let i = 0; i < data.portals.items.length; i++){
        if(data.portals.items[i].type === 24){ // check if its an entrance
          const exitID = parseInt(data.portals.items[i].subpoi.replace('[', '').replace(']', ''));
          const theExit = getExitInfo(data.portals.items, exitID);
          const portalCoords = [
            {lat: parseFloat(data.portals.items[i].position.lat()), lng: parseFloat(data.portals.items[i].position.lng())}, // the entrance
            {lat: theExit.position.lat(), lng: theExit.position.lng()}
          ];

          const portalPath = new google.maps.Polyline({
            path: portalCoords,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
          });

          data.portalNetwork.items.push(portalPath);
        }
      }
    };

    var createMarkers = function(arr){
        arr.forEach(function(item){
          let overlay = 'pois';
          let image = 'poi-default';
          item.type = parseInt(item.type);
            // find out which icon we should show for which mob. The overlay is the array that
            // contains 'buckets' of pois.
            switch(item.type){
                case 1:
                  image = 'poi-mob-' + item.difficulty;
                  if(item.difficulty === '3bff00' || item.difficulty === '90ff00'){
                    overlay = 'mobs-easy';
                  }else if(item.difficulty === 'e5ff00' || item.difficulty === 'ffc300'){
                    overlay = 'mobs-medium';
                  }else if(item.difficulty === 'ff6e00' || item.difficulty === 'ff1900'){
                    overlay = 'mobs-hard';
                  }else if(item.difficulty === 'fff17d'){
                    overlay = 'mobs-boss';
                  }else{ // dont really want the bosses to be the default if messed up.
                    overlay = 'pois';
                    image = 'poi-mob';
                    throw new Error('Mob does not have a correct color (difficulty) value: ' + item);
                  }
                  break;
                case 2: // NPC City
                  overlay = 'cities';
                  image = 'poi-city-npc';
                  break;
                case 3: // Player City
                  overlay = 'playerCities';
                  image = 'poi-city';
                  break;
                case 4: // chaos city
                  overlay = 'cities';
                  image = 'poi-city-chaos';
                  break;
                case 5: // wilderness banks
                  overlay = 'banks';
                  image = 'poi-bank-blue';
                  break;
                case 6: // outposts
                  overlay = 'banks';
                  image = 'poi-bank-red';
                  break;
                case 7: // chaos stones
                  overlay = 'pois';
                  image = 'poi-bind-chaos';
                  break;
                case 8: // resource stash
                  overlay = 'pois';
                  image = 'poi-treasure-chest';
                  break;
                case 9: // hamlets
                  overlay = 'hamlets';
                  image = 'poi-hamlet';
                  break;
                case 10: // villages
                  overlay = 'villages';
                  image = 'poi-village';
                  break;
                case 11: // dungeons
                  overlay = 'dungeons';
                  image = 'poi-chamber-pink';
                  break;
                case 12: // selentine chest
                  overlay = 'pois';
                  image = 'poi-key-selentine';
                  break;
                case 13: // veilron chest
                  overlay = 'pois';
                  image = 'poi-key-veilron';
                  break;
                case 14: // neithel chest
                  overlay = 'pois';
                  image = 'poi-key-neithal';
                  break;
                case 15: // Leenspar chest
                  overlay = 'pois';
                  image = 'poi-key-leenspar';
                  break;
                case 16: // theyril chest
                  overlay = 'pois';
                  image = 'poi-key-theyril';
                  break;
                case 17: // Capital City
                  overlay = 'cities';
                  image = 'poi-city-capital';
                  break;
                case 18: // food crate
                  overlay = 'pois';
                  image = 'poi-food-crate';
                  break;
                case 19: // Magicians box
                  overlay = 'pois';
                  image = 'poi-magician-box';
                  break;
                case 20: // tool box
                  overlay = 'pois';
                  image = 'poi-tool-box';
                  break;
                case 21: // treasure chest
                  overlay = 'pois';
                  image = 'poi-treasure-chest';
                  break;
                case 22: // weapon chest
                  overlay = 'pois';
                  image = 'poi-weapon-chest';
                  break;
                case 23: // armor rack
                  overlay = 'pois';
                  image = 'poi-armor-rack';
                  break;
                case 24: // Portal Network entrance
                  overlay = 'portals';
                  image = 'poi-chamber-entrance';
                  break;
                case 25: // Portal Network exit
                  overlay = 'portals';
                  image = 'poi-chamber-exit';
                  break;
                case 26: // Barrel of Fish
                  overlay = 'pois';
                  image = 'poi-fish-barrel';
                  break;
                default:
                  overlay = 'pois';
                  image = 'poi-default';
            }
            if(typeof item.x === 'string'){
                item.x = parseFloat(item.x);
                item.y = parseFloat(item.y);
            }
            var myLatLng = {lat: item.x, lng: item.y};

            var poiMarker = new google.maps.Marker({
                position: myLatLng,
                map: null,
                icon: location.origin + (location.origin.match('staging|localhost') ? '/images/' : '/map/images/') + image + '.png'
                // title: (item.name) ? item.name : 'pid: ' + item.id
            });
            poiMarker.pid = item.id;
            poiMarker.color = (item.difficulty) ? item.difficulty : 'FFFFFF';
            poiMarker.type = item.type;
            poiMarker.notes = item.notes;
            poiMarker.name = item.name;

            if(item.subpoi){
              poiMarker.subpoi = item.subpoi;
            }

            /*eslint-disable*/
            var mapLabel = new MapLabel({
              text: (item.name) ? item.name : 'pid: ' + item.id,
              position: new google.maps.LatLng(item.x, item.y),
              map: null,
              fontSize: 14,
              strokeWeight: 7,
              align: 'center',
              fontColor: '#' + item.difficulty
            });
            /*eslint-enable*/
            mapLabel.set('position', new google.maps.LatLng(item.x, item.y));

            // if(autoShowLabels){ // if autoshow flag is on
            //   mapLabel.bindTo('map', poiMarker);
            // }
            mapLabel.bindTo('position', poiMarker);
            poiMarker.label = mapLabel;

            if(item.mobcount && item.mobcount !== '0'){
              poiMarker.name = poiMarker.name + ' (' + item.mobcount + ')';
              mapLabel.setOptions({
                text: mapLabel.getName() + ' (' + item.mobcount + ')'
              });
            }

            poiMarker.addListener('click', function(){
              const delay = $('.sidebar').hasClass('active') ? 0 : 500;
              $('.sidebar').addClass('active');
              setTimeout(() => {
                updateInformationWindow(this);
              }, delay);
            });

            poiMarker.addListener('mouseover', function(){
              if(!autoShowLabels){
                this.label.setMap(map);
              }
            });

            poiMarker.addListener('mouseout', function(){
              if(!autoShowLabels){
                this.label.setMap(null);
              }
            });

            data[overlay].items.push(poiMarker);
        });

        createPortalNetworks();
    };

    var getNormalizedCoord = function(coord, zoom) {
        if (!repeatOnXAxis) { return coord; }

        var y = coord.y;
        var x = coord.x;

        // tile range in one direction range is dependent on zoom level
        // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
        var tileRange = 1 << zoom;

        // don't repeat across Y-axis (vertically)
        if (y < 0 || y >= tileRange) {
            return null;
        }

        // repeat across X-axis
        if (x < 0 || x >= tileRange) {
            x = (x % tileRange + tileRange) % tileRange;
        }

        return {
            x: x,
            y: y
        };
    };

    var init = function(){
        // $('#map').width($('#map').height());
        // $('.coords').css({marginLeft: window.innerWidth - $('#map').width()});

        // Define our custom map type
        var customMapType = new google.maps.ImageMapType({
            getTileUrl: function(coord, zoom) {
                var normalizedCoord = getNormalizedCoord(coord, zoom);
                if(normalizedCoord && (normalizedCoord.x < Math.pow(2, zoom)) && (normalizedCoord.x > -1) && (normalizedCoord.y < Math.pow(2, zoom)) && (normalizedCoord.y > -1)) {
                    return imgPath + zoom + '_' + normalizedCoord.x + '_' + normalizedCoord.y + '.jpg';
                } else {
                    return imgPath + 'empty.jpg';
                }
            },
            maxZoom: 7,
            tileSize: new google.maps.Size(256, 256),
            name: 'Darkfall Online World Map'
        });

        // Basic options for our map
        var myOptions = {
            zoom: 4,
            minZoom: 3,
            isPng: true,
            streetViewControl: false,
            center: new google.maps.LatLng(0, 0),
            mapTypeControl: false,
            mapTypeControlOptions: {
                mapTypeIds: ['custom', google.maps.MapTypeId.ROADMAP],
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
        };

        var strictBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(-66, -89),
            new google.maps.LatLng(66, 89)
        );

        // Init the map and hook our custom map type to it
        map = new google.maps.Map(document.getElementById('map'), myOptions);
        map.mapTypes.set('custom', customMapType);
        map.setMapTypeId('custom');

        map.addListener('click', function(event){
          if($('.sidebar').hasClass('active')){
            $('.sidebar').removeClass('active');
            return;
          }
          if(userMarker){
            userMarker.setPosition(event.latLng);
          }else{
            placeMarker(event.latLng);
          }
          window.location.hash = event.latLng.lat() + '%7C' + event.latLng.lng();
          window.ga('send', 'event', 'CustomMarker', 'input', window.location.hash);
          // $('.coords').html(event.latLng.lat() + ', ' + event.latLng.lng()).slideDown();
        });

        map.addListener('rightclick', function(event){
          if(userMarker){
            userMarker.setPosition(event.latLng);
          }else{
            placeMarker(event.latLng);
          }
          window.location.hash = event.latLng.lat() + '%7C' + event.latLng.lng();
          $('#poiNum').val('');
          $('.pidContainer').hide();
          $('#feedbackForm').find('#poiLoc').val(location.hash.substring(1));
          $('.poiLocContainer').show();
          // the difference here
          $('#feedbackForm').modal('show');
        });

        map.addListener('center_changed', function() {
           if(strictBounds.contains(map.getCenter())) {
                return;
            }
            var mapCenter = map.getCenter();
            var X = mapCenter.lng();
            var Y = mapCenter.lat();

            var AmaxX = strictBounds.getNorthEast().lng();
            var AmaxY = strictBounds.getNorthEast().lat();
            var AminX = strictBounds.getSouthWest().lng();
            var AminY = strictBounds.getSouthWest().lat();

            if (X < AminX) {X = AminX; }
            if (X > AmaxX) {X = AmaxX; }
            if (Y < AminY) {Y = AminY; }
            if (Y > AmaxY) {Y = AmaxY; }

            map.setCenter(new google.maps.LatLng(Y, X));
        });

        setupBinds();
    };

    var placeMarker = function(latLng){
      userMarker = new google.maps.Marker({
          position: latLng,
          map: map,
          title: 'Your Marker'
        });
      userMarker.addListener('click', function(){
        $('#poiNum').val('');
        $('.pidContainer').hide();
        $('#feedbackForm').find('#poiLoc').val(location.hash.substring(1));
        $('.poiLocContainer').show();
        $('#feedbackForm').modal('show');
      });
    };

    $.ajax({
      url: location.origin.match('staging|localhost') ? 'http://wiki.riseofagon.info/map/getData.php' : 'getData.php',
      success: function (response) {
        if(response.mobs){
          mobs = response.mobs;
          pois = response.pois;
          createMarkers(pois);
        }else{
          throw new Error(['Spy sappin\' my sentry! - Unable to load Map POIs']);
        }
      },
      error: function (err) {
        // console.log(arguments);
        console.log(err.statusText + ': Spy sappin\' my sentry!');
      }
    });

    init();

})(jQuery);
