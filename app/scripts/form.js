'use strict';

(function($) {
  // Get the form.
  var form = $('#ajax-contact');

  // Get the messages div.
  var formMessages = $('#form-messages');

  var clearForm = function(){
    $('#name').val('');
    $('#email').val('');
    $('#message').val('');
    $('#subject').val('other');
    $('input[name=mid]').val('');
    $('input[name=pid]').val('');
    $('#poiLoc').val('');
    $('#form-messages').text('').removeClass('success error');
    $('button[type=submit]').removeClass('disabled');
  };

  // modal events used are from: http://getbootstrap.com/javascript/#modals-events
  $('#feedbackForm').on('hidden.bs.modal', function () {
    clearForm();
  });

  $('textarea#message').on('focus', function(){
    if(this.innerHTML === 'You guys rock! You simply are amazing and I just wanna...'){
      this.innerHTML = '';
    }
  });

  // Set up an event listener for the contact form.
  $(form).submit(function(event) {
    // Stop the browser from submitting the form.
    event.preventDefault();

    if($('button[type=submit]').hasClass('disabled')){
      return false;
    }

    $('button[type=submit]').addClass('disabled');

    var disabled = $(form).find(':input:disabled').removeAttr('disabled');

    // Serialize the form data.
    var formData = $(form).serialize();

    disabled.attr('disabled', 'disabled');

    // Submit the form using AJAX.
    $.ajax({
        type: 'POST',
        url: 'mailer.php',
        data: formData
    }).done(function(response) {
        // Make sure that the formMessages div has the 'success' class.
        $(formMessages).removeClass('error');
        $(formMessages).addClass('success');

        // Set the message text.
        $(formMessages).text(response);

        // Clear the form.
        clearForm();

        setTimeout(function(){
          $('#feedbackForm').modal('hide');
        }, 1000);
    }).fail(function(data) {
        // Make sure that the formMessages div has the 'error' class.
        $(formMessages).removeClass('success');
        $(formMessages).addClass('error');
        $('button[type=submit]').removeClass('disabled');

        // Set the message text.
        if (data.responseText !== '') {
            $(formMessages).text(data.responseText);
        } else {
            $(formMessages).text('Oops! An error occured and your message could not be sent.');
        }
    });
  });
})(jQuery);
