# Darkfall Info Map
So we never thought we would release this to the public, but here we are.

## Contributers
* [RagnarokDel](http://twitter.com/RagnarokDel)
* [RedSeal](http://twitter.com/iamredseal)

## About
This project will be hard to get going again.
* You will need Application IDs from Google to use the map.
* You will need all the images for the map (there were too many to get into this repo)

## Rights
You can use whatever you want from this, but Rag and Red will not be available to support issues. Consider this a rusty ol' musclecar that you gotta put the work in to fix and get running again.

## DB
We've exported the database and put it into the root here. Feel free to use.
